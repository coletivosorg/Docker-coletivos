FROM debian:unstable-slim
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get -y install nginx php7.0 php7.0-fpm php7.0-mysql nano curl php7.0-xml cron php7.0-curl

COPY entrypoint.sh /entrypoint.sh

RUN touch /var/log/cron.log
CMD cron && tail -f /var/log/cron.log

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]
